package com.example.gallerytv

import android.graphics.drawable.Drawable
import android.support.v17.leanback.widget.ImageCardView
import android.support.v17.leanback.widget.Presenter
import android.support.v4.content.ContextCompat
import android.view.ViewGroup
import com.example.gallerytv.logic.ImageData
import com.squareup.picasso.Picasso

class PhotoPresenter(private val width: Int, private val height: Int) : Presenter() {
    private var defaultImage: Drawable? = null
    private var selectedBackgroundColor: Int = -1
    private var defaultBackgroundColor: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        this.defaultImage = ContextCompat.getDrawable(parent.context, R.drawable.gallery_tv_logo)
        this.selectedBackgroundColor = ContextCompat.getColor(parent.context, R.color.gtv_color_accent)
        this.defaultBackgroundColor = ContextCompat.getColor(parent.context, R.color.gtv_color_accent)

        val cardView = object : ImageCardView(parent.context) {
            override fun setSelected(selected: Boolean) {
                updateCardBackgroundColor(this, selected)
                super.setSelected(selected)
            }
        }

        cardView.isFocusable = true
        cardView.isFocusableInTouchMode = true
        updateCardBackgroundColor(cardView, false)
        return Presenter.ViewHolder(cardView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, item: Any?) {
        val cardView = viewHolder.view as ImageCardView
        val imageData = item as ImageData

        cardView.setMainImageDimensions(this.width, this.height)
        cardView.titleText = imageData.name
        Picasso.get()
            .load(imageData.resource)
            .placeholder(this.defaultImage!!)
            .into(cardView.mainImageView)
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder) {
        val cardView = viewHolder.view as ImageCardView
        cardView.mainImage = null
    }

    private fun updateCardBackgroundColor(view: ImageCardView, selected: Boolean) {
        val color = if (selected) selectedBackgroundColor else defaultBackgroundColor

        view.setBackgroundColor(color)
        view.setInfoAreaBackgroundColor(color)
    }


}