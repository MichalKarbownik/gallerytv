package com.example.gallerytv

import android.app.Activity
import android.os.Bundle
import com.example.gallerytv.logic.ImageData
import com.example.gallerytv.utils.INTENT_EXTRA_IMAGE_KEY
import kotlinx.android.synthetic.main.activity_single_image.*

class SingleImageActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_image)
        val imageData = intent.getParcelableExtra(INTENT_EXTRA_IMAGE_KEY) as ImageData
        single_image_image_view.setImageResource(imageData.resource)
        single_image_button_back.setOnClickListener { finishAfterTransition() }
    }
}