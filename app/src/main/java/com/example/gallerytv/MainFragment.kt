package com.example.gallerytv

import android.content.Intent
import android.os.Bundle
import android.support.v17.leanback.app.BrowseFragment
import android.support.v17.leanback.widget.*
import com.example.gallerytv.logic.ImageData
import com.example.gallerytv.utils.INTENT_EXTRA_IMAGE_KEY

@Suppress("DEPRECATION")
class MainFragment : BrowseFragment() {
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        initUI()
        addItems()
        onItemViewClickedListener = ItemViewClickedListener()
    }

    private fun initUI() {
        title = getString(R.string.gtv_main_title)

        headersState = HEADERS_ENABLED
        isHeadersTransitionOnBackEnabled = true
    }

    private fun addItems() {
        val rowsAdapter = ArrayObjectAdapter(ListRowPresenter())
        val presenter = PhotoPresenter(400, 300)

        val singleRowAdapter = ArrayObjectAdapter(presenter)

        singleRowAdapter.add(ImageData("Mountains", R.drawable.image1))
        singleRowAdapter.add(ImageData("Mirage", R.drawable.image2))
        singleRowAdapter.add(ImageData("Shore", R.drawable.image3))
        singleRowAdapter.add(ImageData("Balloons", R.drawable.image4))
        singleRowAdapter.add(ImageData("Forest", R.drawable.image5))
        singleRowAdapter.add(ImageData("Bridge", R.drawable.image6))
        singleRowAdapter.add(ImageData("Road", R.drawable.image7))

        val gridItemPresenterHeader = HeaderItem(getString(R.string.gtv_main_category))

        rowsAdapter.add(ListRow(gridItemPresenterHeader, singleRowAdapter))

        adapter = rowsAdapter
    }

    private inner class ItemViewClickedListener : OnItemViewClickedListener {
        override fun onItemClicked(itemViewHolder: Presenter.ViewHolder, item: Any,
            rowViewHolder: RowPresenter.ViewHolder, row: Row) {
            val singleImageActivity = Intent(activity, SingleImageActivity::class.java)
            singleImageActivity.putExtra(INTENT_EXTRA_IMAGE_KEY, item as ImageData)

            activity.startActivity(singleImageActivity)
        }
    }
}
